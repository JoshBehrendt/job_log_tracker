//comment!!!!

package main

import (
	"bufio"
	"fmt"
	htemplate "html/template"
	"io"
	"job-log-tracker/backend/logLine"
	"job-log-tracker/backend/printJob"
	"job-log-tracker/backend/unzip"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
)

var jobIDVersion []string = []string{"jobID=#,", "JobId=#,", "job: #|", "job:#|", "job ID: # ",
	"print job #|", "job '#'.", "job id # ", "job ID: # ", "job #|"}

var upFileName string
var AppLineList []logLine.LogLine
var PrintJobList []printJob.PrintJob
var PrintJobMap map[int]printJob.PrintJob
var ServerLogList []string

var PaperCutUser string = "SYSTEM"
var PCutUserDone bool = false

var templatePath = getBaseDir() + "../../../frontend/app/build"
var templates = htemplate.Must(htemplate.ParseFiles(
	filepath.Join(templatePath, "index.html")))

//var JobListTemplate htemplate

type JobPage struct {
	JMap  map[int]printJob.PrintJob
	First string // the selected starting time
	Last  string // the selected finishing time
}

type logPage struct {
	Job         printJob.PrintJob         `json:"job"`
	HasRedirect bool                      `json:"hasRedirect"`
	ReJob       printJob.PrintJob         `json:"redirectJob"`
	LogLines    map[int32]logLine.LogLine `json:"logLines"`
}

// We'll create a list of print jobs
var printJobs = []printJob.PrintJob{}

// Get the absolute path from a relative path, used to get the abs path of a file
func getBaseDir() string {
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)

	return basepath
}

func getAbsPath(path string) string {
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	absPath, e := filepath.Abs(basepath + "/" + path)
	if e == nil {
		return absPath
	}
	return ""
}

// define our WebSocket endpoint
func serveWs(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.Host)

}

func main() {
	fmt.Println("Time with MicroSeconds: ", time.Now().Format("2006-01-02 15:04:05.000"))

	router := gin.Default()

	// Serve frontend static files
	router.Use(static.Serve("/", static.LocalFile(os.Getenv("HOME_DIR")+"/../frontend/app/build", true)))

	// Setup route group for the API
	api := router.Group("/api")
	{
		api.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
		})
		api.GET("/logLines/:jobUid", func(c *gin.Context) {
			jid := c.Param("jobUid")
			fmt.Println(jid)
			var logPageDetails logPage
			var jobtemp printJob.PrintJob
			for i := 0; i < len(PrintJobList); i++ {
				if PrintJobList[i].JobUID == jid {
					jobtemp = PrintJobList[i]
					i = len(PrintJobList)
				}
			}
			logPageDetails.Job = jobtemp
			listLogs(jobtemp, &logPageDetails)
			c.JSON(http.StatusOK, logPageDetails)
		})
	}

	router.POST("/upload", func(c *gin.Context) {
		AppLineList = nil
		ServerLogList = nil
		// // single file
		// file, _ := c.FormFile("file")
		// log.Println(file.Filename)

		// // Upload the file to specific dst.
		// c.SaveUploadedFile(file, os.Getenv("HOME_DIR")+"/main/Uploaded-files")

		// c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
		// Multipart form
		form, err := c.MultipartForm()

		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("get form err: %s", err.Error()))
			return
		}

		files := form.File["upload log file"]

		for _, file := range files {
			log.Println(file.Filename)
			fileOpen, _ := file.Open()

			// Upload the file to specific dst.
			upFileName = os.Getenv("HOME_DIR") + "/main/Uploaded-files/" + file.Filename
			errr := os.Remove(upFileName)
			if errr != nil {
				fmt.Println("No previous file to delete")
			} else {
				fmt.Println("Previous file deleted")
			}

			f, err := os.OpenFile(upFileName, os.O_WRONLY|os.O_CREATE, 0666)
			if err != nil {
				fmt.Println(err)
				return
			}
			defer f.Close()
			io.Copy(f, fileOpen)
			upFile, err := os.Open(upFileName)
			fileNameSplit := strings.Split(file.Filename, ".")
			fullFile := getAbsPath("Uploaded-files/" + file.Filename)
			if fileNameSplit[len(fileNameSplit)-1] == "zip" {
				ServerLogList = unzip.UnzipFile(fullFile)
			}

			if len(ServerLogList) > 0 {
				fmt.Println("Zip file uploaded")
				ServerLogList = orderList(ServerLogList)

				for i := 0; i < len(ServerLogList); i++ {
					upFile, err = os.Open(ServerLogList[len(ServerLogList)-1-i])
					if err != nil {
						fmt.Println(err)
					}
					scanner := bufio.NewScanner(upFile)
					var counter int32 = 0
					for scanner.Scan() {
						counter++
						fmt.Println(upFile.Name() + " Couting line: " + fmt.Sprint(counter))
						createLine(scanner.Text(), upFile.Name(), counter)
					}
				}
			} else {
				fmt.Println("Log file uploaded")
				scanner := bufio.NewScanner(upFile)
				var counter int32 = 0
				for scanner.Scan() {
					counter++
					fmt.Println(upFile.Name() + " Couting line: " + fmt.Sprint(counter))
					createLine(scanner.Text(), upFile.Name(), counter)
				}
			}

		}
		// c.String(http.StatusOK, fmt.Sprintf("%d files uploaded!", len(files)))

		startDate, endDate := listJobs()
		fmt.Println(len(PrintJobList), " jobs found")
		fmt.Println(startDate, ":", endDate)
		// p := JobPage{
		// 	JMap:  PrintJobMap,
		// 	First: startDate.Format("2006-01-02T15:04:05Z"),
		// 	Last:  endDate.Format("2006-01-02T15:04:05Z"),
		// }
		c.JSON(http.StatusOK, PrintJobList)
	}) //Upload end

	// Start and run the server
	api.GET("/jobs", JobHandler)

	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "8080" //Default port is 8080
	}

	router.Use(cors.Default())
	router.Run(":" + port)

	// Serve frontend static files
	router.Use(static.Serve("/", static.LocalFile(os.Getenv("HOME_DIR")+"../frontend/app/build", true)))

	// Setup route group for the API
	api = router.Group("/api")
	{
		api.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
		})
	}

	// // Start and run the server
	// api.GET("/jobs", JobHandler)

	// port, ok = os.LookupEnv("PORT")
	// if !ok {
	// 	port = "443" //Default port is 8080
	// }
	// router.Run(":" + port)
}

// RoomHandler retrieves a list of available rooms
func JobHandler(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.JSON(http.StatusOK, printJobs)
}

func orderList(l []string) (result []string) {
	for i := 0; i < len(l); i++ {
		sSplit := strings.Split(l[i], ".")
		num := 0
		if sSplit[len(sSplit)-1] == "log" {
			num = 0
		} else {
			numk, _ := strconv.Atoi(sSplit[len(sSplit)-1])
			num = numk
		}
		for j := 0; j < i; j++ {
			if j == i {
				j++
			}
			lSplit := strings.Split(l[j], ".")
			lnum := 0
			if lSplit[len(lSplit)-1] == "log" {
				lnum = 0
			} else {
				lnumk, _ := strconv.Atoi(lSplit[len(lSplit)-1])
				lnum = lnumk
			}
			if num < lnum {
				val := l[i]
				l = append(l[:i], l[i+1:]...)

				newSlice := make([]string, j+1)
				copy(newSlice, l[:j])
				newSlice[j] = val

				l = append(newSlice, l[j:]...)
				j = len(l)
			}
		}
	}
	return l
}

func createLine(LineText string, dir string, counter int32) {

	SplitLine := strings.Split(LineText, " ")
	if !PCutUserDone && len(SplitLine) > 1 {
		if SplitLine[1] == "Application" {
			for i := 0; i < len(SplitLine); i++ {
				if SplitLine[i] == "User:" {
					PaperCutUser = SplitLine[i+1]
				}
			}
			PCutUserDone = true
		} else if len(SplitLine) > 6 {
			if SplitLine[6] == "Starting" && SplitLine[7] == "application" {
				for i := 0; i < len(SplitLine); i++ {
					if SplitLine[i] == "User:" {
						PaperCutUser = SplitLine[i+1]
					}
				}
				PCutUserDone = true
			}
		}
	}

	if len(SplitLine) > 3 {
		var temp string = ""
		for i := 3; i < len(SplitLine); i++ {
			temp += SplitLine[i] + " "
		}
		SplitLine[3] = temp
		layout := "2006-01-02T15:04:05.000"
		SplitLine[1] = strings.ReplaceAll(SplitLine[1], ",", ".")
		str := SplitLine[0] + "T" + SplitLine[1]
		LineDateTime, err := time.Parse(layout, str)
		if err != nil {
			//fmt.Println(err)
			return
		}
		TypeTemp := logLine.Other
		switch SplitLine[2] {
		case "DEBUG":
			TypeTemp = logLine.Debug
		case "ERROR":
			TypeTemp = logLine.Error
		case "INFO":
			TypeTemp = logLine.Info
		default:
			TypeTemp = logLine.Other
		}

		LineTemp := logLine.LogLine{
			LineTime:   LineDateTime,
			TypeOfLine: TypeTemp,
			LineString: SplitLine[3],
			LineNumber: counter,
		}

		if strings.Contains(LineTemp.LineString, "Initial print event details") {
			LineTemp.InitialPrintEvent = true
		} else {
			LineTemp.InitialPrintEvent = false
			if strings.Contains(LineTemp.LineString, "Finished populating spooled job details.") || strings.Contains(LineTemp.LineString, "Processing print event:") {
				LineTemp.FinishedPop = true
			} else {
				LineTemp.FinishedPop = false
			}
		}
		dirsplit := strings.Split(dir, "\\")
		LineTemp.LogFileDir = dirsplit[len(dirsplit)-1]
		AppLineList = append(AppLineList, LineTemp)
	}
}

func listJobs() (time.Time, time.Time) {
	var startDate time.Time = time.Time{}
	var endDate time.Time = time.Time{}

	PrintJobMap = make(map[int]printJob.PrintJob)
	PrintJobList = nil
	for _, Line := range AppLineList {
		if startDate.IsZero() {
			startDate = Line.LineTime
		} else if endDate.IsZero() {
			endDate = Line.LineTime
		} else {
			if Line.LineTime.Before(startDate) {
				startDate = Line.LineTime
			}
			if Line.LineTime.After(endDate) {
				endDate = Line.LineTime
			}
		}
		if Line.InitialPrintEvent || Line.FinishedPop {
			var tempjob printJob.PrintJob
			tempjob.ReleaseType = false
			templine := Line.LineString
			if Line.FinishedPop {
				templine = getStringInBetween(templine, " PrintEvent[", "] ")
				//fmt.Println("Finished: ", templine)
			}
			tempsplit := strings.Split(templine, "details. ")

			if len(tempsplit) > 1 {
				templine = tempsplit[1]
			}
			splitTest := strings.Split(templine, ",")
			tempjob.LineNumber = Line.LineNumber
			for i := 0; i < len(splitTest); i++ {
				lineSplit := strings.Split(splitTest[i], "=")
				switch lineSplit[0] {
				case "jobID":
					s, err := strconv.Atoi(lineSplit[1])
					if err == nil {
						tempjob.IDNumber = s
					}
				case "time":
					if Line.InitialPrintEvent {
						s, err := time.Parse("2006-01-02 15:04:05", lineSplit[i])
						if err == nil {
							tempjob.StartTime = s
						}
						tempjob.StartTimeString = lineSplit[i]
					}
				case "user":
					tempjob.Username = lineSplit[1]
				case "lang":
					if tempjob.Language == "" || tempjob.Language == "Unknown" {
						tempjob.Language = lineSplit[1]
					}
				case "jobUid":
					tempjob.JobUID = strings.Split(lineSplit[1], ".")[0]
				case "eventID":
					tempjob.EventID = strings.Split(lineSplit[1], "|")[1]
				case "pages":
					s, err := strconv.Atoi(lineSplit[1])
					if err == nil {
						tempjob.PageCount = s
					}
				case "copyCount":
					s, err := strconv.Atoi(lineSplit[1])
					if err == nil {
						tempjob.CopyCount = s
					} else {
						s2 := strings.Split(lineSplit[1], "paperSize")
						s, err := strconv.Atoi(s2[0])
						if err == nil {
							tempjob.CopyCount = s
						}
					}
				case "docName":
					if tempjob.DocumentName == "" {
						tempjob.DocumentName = lineSplit[1]
					}
				case "printer":
					tempjob.Printer = lineSplit[1]
				}

			}
			newJob := true
			//Check if it's a new job or updated details for an old one
			for i := 0; i < len(PrintJobList); i++ {
				if PrintJobList[i].IDNumber == tempjob.IDNumber {
					if PrintJobList[i].DocumentName == "" {
						PrintJobList[i].DocumentName = tempjob.DocumentName
					} else if tempjob.DocumentName == "" {
						tempjob.DocumentName = PrintJobList[i].DocumentName
					}
					if PrintJobList[i].JobUID == tempjob.JobUID {
						PrintJobList[i] = tempjob
						PrintJobMap[tempjob.IDNumber] = tempjob
						newJob = false
					} else if PrintJobList[i].DocumentName == tempjob.DocumentName || PrintJobList[i].Username == PaperCutUser {
						newJob = false
						PrintJobList[i].PageCount = tempjob.PageCount
						PrintJobList[i].Username = tempjob.Username
						PrintJobList[i].Language = tempjob.Language
						PrintJobMap[tempjob.IDNumber] = PrintJobList[i]
					}
				}
			}

			if newJob {
				PrintJobList = append(PrintJobList, tempjob)
				PrintJobMap[tempjob.IDNumber] = tempjob
			}
		}
	}
	fmt.Println("Start Time: ", startDate.Format("2006-01-02 15:04:05"))
	fmt.Println("End Time: ", endDate.Format("2006-01-02 15:04:05"))
	return startDate, endDate
}

func getStringInBetween(str string, start string, end string) (result string) {
	s := strings.Index(str, start)
	if s == -1 {
		return
	}
	s += len(start)
	e := strings.Index(str, end)
	return str[s:e]
}

func listLogs(v printJob.PrintJob, logPageDetails *logPage) {
	(*logPageDetails).LogLines = make(map[int32]logLine.LogLine)
	(*logPageDetails).HasRedirect = false
	fmt.Println("JobID: ", v.IDNumber)
	hasRedirect := false
	jobUID := v.JobUID
	fmt.Println("JobUID: ", jobUID)
	redirectUID := ""
	redirectJob := printJob.PrintJob{}
	var redirectLine int32 = 0
	var jobIDVersionNum []string
	for _, temp := range jobIDVersion {
		temp = strings.ReplaceAll(temp, "#", strconv.Itoa(v.IDNumber))
		jobIDVersionNum = append(jobIDVersionNum, temp)
	}
	for _, line := range AppLineList {
		if line.LineTime.After(v.StartTime) {
			for _, substring := range jobIDVersionNum {
				contains := false
				if strings.Contains(line.LineString, substring) || strings.Contains(line.LineString, jobUID) {
					contains = true

					(*logPageDetails).LogLines[line.LineNumber] = line
					if strings.Contains(line.LineString, "Releasing job:") {
						relSting := strings.Split(line.LineString, "(")
						tempjob := v
						if strings.Contains(line.LineString, "Device:") {
							tempjob.ReleaseType = false
							ts := (strings.Split(line.LineString, "Device:"))[1]
							ts = (strings.Split(ts, ")"))[0]
							tempjob.ReleaseInfo = ts
						} else {
							tempjob.ReleaseInfo = (strings.Split(relSting[len(relSting)-1], ":"))[1]
							switch (strings.Split(relSting[len(relSting)-1], ":"))[0] {
							case "Device":
								tempjob.ReleaseType = false
								tempjob.ReleaseInfo = (strings.Split(tempjob.ReleaseInfo, ")"))[0]
							default:
								tempjob.ReleaseType = true
								tempjob.ReleaseInfo = strings.ReplaceAll((strings.Split(tempjob.ReleaseInfo, ","))[0], "'", "")
							}
						}

						for i := 0; i < len(PrintJobList); i++ {
							if PrintJobList[i].JobUID == v.JobUID {
								PrintJobList[i] = tempjob
								v = tempjob
								i = len(PrintJobList)
							}
						}
						PrintJobMap[v.IDNumber] = tempjob
					}

					if strings.Contains(line.LineString, "instructed to: REDIRECT") && redirectUID == "" {
						hasRedirect = true
						redirectLine = line.LineNumber
						lineSplit := strings.Split(line.LineString, ";")
						for _, s := range lineSplit {
							if strings.Contains(s, "redirect-uid") {
								redirectUID = strings.Split(s, "=")[1]
							} else if strings.Contains(s, "printer") && redirectUID == "" {
								redirectJob.Printer = strings.Split(s, "=")[1]
							}
						}
					}
				} else if hasRedirect {
					if strings.Contains(line.LineString, "Validating redirected job UID: "+redirectUID) {
						hasRedirect = false
						linesplit := strings.Split(line.LineString, ":")
						tempstring := strings.Split(linesplit[3], "(")
						tempstring[0] = strings.ReplaceAll(tempstring[0], " ", "")

						if testint, err := strconv.Atoi(tempstring[0]); err == nil {
							tempjob := v
							for i := 0; i < len(PrintJobList); i++ {
								if PrintJobList[i].JobUID == v.JobUID {
									tempjob = PrintJobList[i]
									i = len(PrintJobList)
								}
							}
							tempjob.RedirectJobID = testint
							tempjob.RedirectType = false
							for i := 0; i < len(PrintJobList); i++ {
								if PrintJobList[i].JobUID == v.JobUID {
									PrintJobList[i] = tempjob
									i = len(PrintJobList)
								}
							}
							PrintJobMap[v.IDNumber] = tempjob
							(*logPageDetails).Job = v
							redirectJob.IDNumber = testint
							redirectJob.RedirectType = true
							redirectJob.RedirectJobID = tempjob.IDNumber
							redirectJob.Username = tempjob.Username
							redirectJob.DocumentName = tempjob.DocumentName
							redirectJob.PageCount = tempjob.PageCount
						}

					}

				}
				if contains {
					break
				}
			}
		}
	}
	if redirectUID != "" {
		redirectJob.LineNumber = redirectLine
		trackRedirectedJob(redirectJob, logPageDetails)
	}
}

func trackRedirectedJob(redirectJob printJob.PrintJob, logPageDetails *logPage) {
	var jobIDVersionNum []string
	redirectJob.ReleaseType = (*logPageDetails).Job.ReleaseType
	redirectJob.ReleaseInfo = (*logPageDetails).Job.ReleaseInfo
	(*logPageDetails).ReJob = redirectJob
	(*logPageDetails).HasRedirect = true
	for _, temp := range jobIDVersion {
		temp = strings.ReplaceAll(temp, "#", strconv.Itoa(redirectJob.IDNumber))
		jobIDVersionNum = append(jobIDVersionNum, temp)
	}
	for _, line := range AppLineList {
		for _, substring := range jobIDVersionNum {
			contains := false
			if strings.Contains(line.LineString, substring) {
				(*logPageDetails).LogLines[line.LineNumber] = line
			}
			if contains {
				break
			}
		}
	}

}
