module job-log-tracker/backend

go 1.13

require (
	github.com/gin-gonic/contrib v0.0.0-20190923054218-35076c1b2bea
	github.com/gin-gonic/gin v1.4.0
	github.com/gorilla/websocket v1.4.1
)
