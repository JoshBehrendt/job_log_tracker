package logLine

import (
	"time"
)

type LineType int

const (
	Debug LineType = iota
	Error
	Info
	Other
)

type LogLine struct {
	LineTime          time.Time `json:"time"`
	LineString        string `json:"lineString"`
	TypeOfLine        LineType `json:"lineType"`
	LineNumber        int32 `json:"lineNumber"`
	InitialPrintEvent bool `json:"initialPrintEvent"`
	FinishedPop       bool `json:"finishedPop"`
	LogFileDir        string `json:"logFileDir"`
}
