package unzip

import (
	"archive/zip"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

func getAbsPath(path string) string {
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	absPath, e := filepath.Abs(basepath + "/" + path)
	if e == nil {
		return absPath
	}
	return ""
}

func UnzipFile(dir string) (result []string) {
	var ServerLogList []string
	zipReader, _ := zip.OpenReader(dir)
	for _, file := range zipReader.File {

		zippedFile, err := file.Open()
		if err != nil {
			log.Fatal(err)
		}
		defer zippedFile.Close()

		targetDir := "./"
		extractedFilePath := filepath.Join(
			targetDir,
			file.Name,
		)

		if file.FileInfo().IsDir() {
			log.Println("Directory Created:", extractedFilePath)
			os.MkdirAll(extractedFilePath, file.Mode())
		} else {
			filePath, fileName := filepath.Split(file.Name)
			if strings.Split(fileName, ".")[0] == "server" && strings.Split(fileName, ".")[1] == "log" {
				extractedFilePath = strings.Replace(dir, ".zip", "", -1) + "\\" + strings.Replace(filePath, "/", "\\", -1)

				if err := os.MkdirAll(extractedFilePath, file.Mode()); err != nil {
					fmt.Println(err)
				}
				extractedFilePath = extractedFilePath + fileName
				outputFile, err := os.OpenFile(
					extractedFilePath,
					os.O_WRONLY|os.O_CREATE|os.O_TRUNC,
					file.Mode(),
				)
				if err != nil {
					log.Fatal(err)
				}
				defer outputFile.Close()

				_, err = io.Copy(outputFile, zippedFile)
				if err != nil {
					log.Fatal(err)
				}
				ServerLogList = append(ServerLogList, extractedFilePath)
			}

		}
	}

	return ServerLogList

}
