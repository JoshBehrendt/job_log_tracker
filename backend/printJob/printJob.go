package printJob

import (
	"time"
)

type RelType int

const (
	None RelType = iota
	Device
	Web
	Other
)

type PrintJob struct {
	IDNumber        int `json:"id" binding:"required"`
	Username        string `json:"username"`
	PageCount       int `json:"pageCount"`
	ColorPages      int `json:"colorPages"`
	CopyCount       int `json:"copyCount"`
	JobUID          string `json:"jobUid"`
	DocumentName    string `json:"documentName"`
	Printer         string `json:"printer"`
	StartTime       time.Time `json:"startTime"`
	StartTimeString string `json:"startTimeString"`
	EndTime         time.Time `json:"endTimeString"`
	LineNumber      int32 `json:"lineNumber"`
	RedirectUID     string `json:"redirectUid"`
	Language        string `json:"language"`
	RedirectType    bool `json:"redirectType"`//false=Original, true=Redirected Job 
	RedirectJobID   int `json:"redirectJobId"`
	ReleaseType     bool `json:"releaseType"`//false=device, true=other
	ReleaseInfo     string `json:"releaseInfo"`
	EventID 		string `json:"eventID"`
}
