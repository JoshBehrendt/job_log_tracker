//comment!!!!

package main

import (
	"bufio"
	"crypto/md5"
	"fmt"
	"html/template"
	htemplate "html/template"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"./backend/logLine"
	"./backend/printJob"
	"./backend/unzip"
)

var jobIDVersion []string = []string{"jobID=#,", "JobId=#,", "job: #|", "job:#|", "job ID: # ", "print job #|", "job '#'.", "job id # ", "job ID: # "}

var upFileName string
var AppLineList []logLine.LogLine
var PrintJobList []printJob.PrintJob
var PrintJobMap map[int]printJob.PrintJob
var ServerLogList []string

var PaperCutUser string = "SYSTEM"
var PCutUserDone bool = false

//var JobListTemplate htemplate

type JobPage struct {
	JMap map[int]printJob.PrintJob
}

type logPage struct {
	Job         printJob.PrintJob
	HasRedirect bool
	ReJob       printJob.PrintJob
	LogLines    map[int32]logLine.LogLine
}

// Get the absolute path from a relative path, used to get the abs path of a file
func getAbsPath(path string) string {
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	absPath, e := filepath.Abs(basepath + "/" + path)
	if e == nil {
		return absPath
	}
	return ""
}

func setupRoutes() {
	templates := htemplate.Must(htemplate.ParseFiles(
		getAbsPath("/frontend/templates/welcome-template.html"),
		getAbsPath("/frontend/templates/JobList.html"),
		getAbsPath("/frontend/templates/logs.html"),
		getAbsPath("/frontend/templates/Details.html")))

	http.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("method:", r.Method)
		if r.Method == "GET" {
			fmt.Println("GET!")
			crutime := time.Now().Unix()
			h := md5.New()
			io.WriteString(h, strconv.FormatInt(crutime, 10))
			token := fmt.Sprintf("%x", h.Sum(nil))

			t, _ := template.ParseFiles("upload.gtpl")
			t.Execute(w, token)
		} else {

			AppLineList = nil
			r.ParseMultipartForm(32 << 20)
			file, handler, err := r.FormFile("upload log file")
			if err != nil {
				fmt.Println("Error")
				fmt.Println(err)
				return
			}
			defer file.Close()
			

			//fmt.Fprintf(w, "%v", handler.Header)
			f, err := os.OpenFile("./Uploaded-files/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
			if err != nil {
				fmt.Println(err)
				return
			}
			defer f.Close()
			io.Copy(f, file)
			upFileName = "./Uploaded-files/" + handler.Filename
			upFile, err := os.Open(upFileName)
			if err != nil {
				fmt.Println(err)
				return
			}
			fmt.Println("File " + upFile.Name())
			defer upFile.Close()
			fileNameSplit := strings.Split(handler.Filename, ".")
			fullFile := getAbsPath("Uploaded-files/"+handler.Filename)
			if fileNameSplit[len(fileNameSplit)-1] == "zip" {
				ServerLogList = unzip.UnzipFile(fullFile)
			}
				if len(ServerLogList)>0{
					for i:=0; i<len(ServerLogList);i++{
					upFile, err = os.Open(ServerLogList[len(ServerLogList)-1])
					if err != nil{
						fmt.Println(err)
					}
					scanner := bufio.NewScanner(upFile)
					var counter int32 = 0
					for scanner.Scan() {
						counter++				
						createLine(scanner.Text(), upFile.Name(), counter)
					}
				}
			} else {
				scanner := bufio.NewScanner(upFile)
				var counter int32 = 0
				for scanner.Scan() {
					counter++
					createLine(scanner.Text(), upFile.Name(), counter)
				}
			}
			

			listJobs()
			fmt.Println(len(PrintJobList), " jobs found")
			p := JobPage{JMap: PrintJobMap}
			if err := templates.ExecuteTemplate(w, "JobList.html", p); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			//http.Redirect(w, r, "localhost:8080/done", 303)
		}
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		//If errors show an internal server error message
		//I also pass the welcome struct to the welcome-template.html file.
		if err := templates.ExecuteTemplate(w, "welcome-template.html", htemplate.HTML("localhost:8080/upload")); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})
	http.HandleFunc("/logs", func(w http.ResponseWriter, r *http.Request) {

		var logPageDetails logPage

		if r.Method == "GET" {
			for k, v := range r.URL.Query() {
				jid, err := strconv.Atoi(v[0])
				if err != nil {
					fmt.Println(err)
					fmt.Println(k)
					return
				}
				logPageDetails.Job = PrintJobMap[jid]
				listLogs(jid, &logPageDetails)
				logPageDetails.Job = PrintJobMap[jid]
			}

		}

		if err := templates.ExecuteTemplate(w, "logs.html", logPageDetails); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})
	http.HandleFunc("/details", func(w http.ResponseWriter, r *http.Request) {
		//If errors show an internal server error message
		//I also pass the welcome struct to the welcome-template.html file.
		if err := templates.ExecuteTemplate(w, "Details.html", htemplate.HTML("localhost:8080/upload")); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	http.ListenAndServe(":8080", nil)
}

func main() {
	fmt.Println("Time with MicroSeconds: ", time.Now().Format("2006-01-02 15:04:05.000"))

	setupRoutes()
}

func createLine(LineText string, dir string, counter int32) {
	SplitLine := strings.Split(LineText, " ")
	if !PCutUserDone && len(SplitLine) > 1 {
		if SplitLine[1] == "Application" {
			for i := 0; i < len(SplitLine); i++ {
				if SplitLine[i] == "User:" {
					PaperCutUser = SplitLine[i+1]
				}
			}
			PCutUserDone = true
		} else if len(SplitLine) > 6 {
			if SplitLine[6] == "Starting" && SplitLine[7] == "application" {
				for i := 0; i < len(SplitLine); i++ {
					if SplitLine[i] == "User:" {
						PaperCutUser = SplitLine[i+1]
					}
				}
				PCutUserDone = true
			}
		}
	}

	if len(SplitLine) > 3 {
		var temp string = ""
		for i := 3; i < len(SplitLine); i++ {
			temp += SplitLine[i] + " "
		}
		SplitLine[3] = temp
		layout := "2006-01-02T15:04:05.000"
		SplitLine[1] = strings.ReplaceAll(SplitLine[1], ",", ".")
		str := SplitLine[0] + "T" + SplitLine[1]
		LineDateTime, err := time.Parse(layout, str)
		if err != nil {
			//fmt.Println(err)
			return
		}
		TypeTemp := logLine.Other
		switch SplitLine[2] {
		case "DEBUG":
			TypeTemp = logLine.Debug
		case "ERROR":
			TypeTemp = logLine.Error
		case "INFO":
			TypeTemp = logLine.Info
		default:
			TypeTemp = logLine.Other
		}

		LineTemp := logLine.LogLine{
			LineTime:   LineDateTime,
			TypeOfLine: TypeTemp,
			LineString: SplitLine[3],
			LineNumber: counter,
		}

		if strings.Contains(LineTemp.LineString, "Initial print event details") {
			LineTemp.InitialPrintEvent = true
		} else {
			LineTemp.InitialPrintEvent = false
			if strings.Contains(LineTemp.LineString, "Finished populating spooled job details.") || strings.Contains(LineTemp.LineString, "Processing print event:") {
				LineTemp.FinishedPop = true
			} else {
				LineTemp.FinishedPop = false
			}
		}
		LineTemp.LogFileDir = dir
		AppLineList = append(AppLineList, LineTemp)
	}
}

func listJobs() {
	PrintJobMap = make(map[int]printJob.PrintJob)
	PrintJobList = nil
	for _, Line := range AppLineList {
		if Line.InitialPrintEvent || Line.FinishedPop {
			var tempjob printJob.PrintJob
			tempjob.ReleaseType = false
			templine := Line.LineString
			if Line.FinishedPop {
				templine = getStringInBetween(templine, " PrintEvent[", "] ")
				//fmt.Println("Finished: ", templine)
			}
			tempsplit := strings.Split(templine, "details. ")

			if len(tempsplit) > 1 {
				templine = tempsplit[1]
			}
			splitTest := strings.Split(templine, ",")
			tempjob.LineNumber = Line.LineNumber
			for i := 0; i < len(splitTest); i++ {
				lineSplit := strings.Split(splitTest[i], "=")
				switch lineSplit[0] {
				case "jobID":
					s, err := strconv.Atoi(lineSplit[1])
					if err == nil {
						tempjob.IDNumber = s
					}
				case "time":
					if Line.InitialPrintEvent {
						s, err := time.Parse("2006-01-02 15:04:05", lineSplit[i])
						if err == nil {
							tempjob.StartTime = s
						}
						tempjob.StartTimeString = lineSplit[i]
					}
				case "user":
					tempjob.Username = lineSplit[1]
				case "lang":
					if tempjob.Language == "" || tempjob.Language == "Unknown" {
						tempjob.Language = lineSplit[1]
					}
				case "jobUid":
					tempjob.JobUID = strings.Split(lineSplit[1], ".")[0]
				case "pages":
					s, err := strconv.Atoi(lineSplit[1])
					if err == nil {
						tempjob.PageCount = s
					}
				case "copyCount":
					s, err := strconv.Atoi(lineSplit[1])
					if err == nil {
						tempjob.CopyCount = s
					} else {
						s2 := strings.Split(lineSplit[1], "paperSize")
						s, err := strconv.Atoi(s2[0])
						if err == nil {
							tempjob.CopyCount = s
						}
					}
				case "docName":
					if tempjob.DocumentName == "" {
						tempjob.DocumentName = lineSplit[1]
					}
				case "printer":
					tempjob.Printer = lineSplit[1]
				}

			}
			newJob := true
			for i := 0; i < len(PrintJobList); i++ {
				if PrintJobList[i].IDNumber == tempjob.IDNumber {
					if PrintJobList[i].DocumentName == "" {
						PrintJobList[i].DocumentName = tempjob.DocumentName
					} else if tempjob.DocumentName == "" {
						tempjob.DocumentName = PrintJobList[i].DocumentName
					}
					if PrintJobList[i].DocumentName == tempjob.DocumentName || PrintJobList[i].Username == PaperCutUser {
						newJob = false
						PrintJobList[i].PageCount = tempjob.PageCount
						PrintJobList[i].Username = tempjob.Username
						PrintJobList[i].Language = tempjob.Language
						PrintJobMap[tempjob.IDNumber] = PrintJobList[i]
					}
				}
			}
			if newJob {
				PrintJobList = append(PrintJobList, tempjob)
				PrintJobMap[tempjob.IDNumber] = tempjob
			}
		}
	}
}

func getStringInBetween(str string, start string, end string) (result string) {
	s := strings.Index(str, start)
	if s == -1 {
		return
	}
	s += len(start)
	e := strings.Index(str, end)
	return str[s:e]
}

func listLogs(v int, logPageDetails *logPage) {
	(*logPageDetails).LogLines = make(map[int32]logLine.LogLine)
	(*logPageDetails).HasRedirect = false
	fmt.Println("JobID: ", v)
	hasRedirect := false
	jobUID := PrintJobMap[v].JobUID
	fmt.Println("JobUID: ", jobUID)
	redirectUID := ""
	redirectJob := printJob.PrintJob{}
	var redirectLine int32 = 0
	var jobIDVersionNum []string
	for _, temp := range jobIDVersion {
		temp = strings.ReplaceAll(temp, "#", strconv.Itoa(v))
		jobIDVersionNum = append(jobIDVersionNum, temp)
	}
	for _, line := range AppLineList {
		for _, substring := range jobIDVersionNum {
			contains := false
			if strings.Contains(line.LineString, substring) || strings.Contains(line.LineString, jobUID) {
				contains = true

				(*logPageDetails).LogLines[line.LineNumber] = line
				if strings.Contains(line.LineString, "Releasing job:") {
					relSting := strings.Split(line.LineString, "(")
					tempjob := PrintJobMap[v]
					if strings.Contains(line.LineString, "Device:") {
						tempjob.ReleaseType = false
						ts := (strings.Split(line.LineString, "Device:"))[1]
						ts = (strings.Split(ts, ")"))[0]
						tempjob.ReleaseInfo = ts
					} else {
						tempjob.ReleaseInfo = (strings.Split(relSting[len(relSting)-1], ":"))[1]
						switch (strings.Split(relSting[len(relSting)-1], ":"))[0] {
						case "Device":
							tempjob.ReleaseType = false
							tempjob.ReleaseInfo = (strings.Split(tempjob.ReleaseInfo, ")"))[0]
						default:
							tempjob.ReleaseType = true
							tempjob.ReleaseInfo = strings.ReplaceAll((strings.Split(tempjob.ReleaseInfo, ","))[0], "'", "")
						}
					}

					PrintJobMap[v] = tempjob
				}

				if strings.Contains(line.LineString, "instructed to: REDIRECT") && redirectUID == "" {
					hasRedirect = true
					redirectLine = line.LineNumber
					lineSplit := strings.Split(line.LineString, ";")
					for _, s := range lineSplit {
						if strings.Contains(s, "redirect-uid") {
							redirectUID = strings.Split(s, "=")[1]
						} else if strings.Contains(s, "printer") && redirectUID == "" {
							redirectJob.Printer = strings.Split(s, "=")[1]
						}
					}
				}
			} else if hasRedirect {
				if strings.Contains(line.LineString, "Validating redirected job UID: "+redirectUID) {
					hasRedirect = false
					linesplit := strings.Split(line.LineString, ":")
					tempstring := strings.Split(linesplit[3], "(")
					tempstring[0] = strings.ReplaceAll(tempstring[0], " ", "")

					if testint, err := strconv.Atoi(tempstring[0]); err == nil {
						tempjob := PrintJobMap[v]
						tempjob.RedirectJobID = testint
						tempjob.RedirectType = false
						PrintJobMap[v] = tempjob

						redirectJob.IDNumber = testint
						redirectJob.RedirectType = true
						redirectJob.RedirectJobID = tempjob.IDNumber
						redirectJob.Username = tempjob.Username
						redirectJob.DocumentName = tempjob.DocumentName
						redirectJob.PageCount = tempjob.PageCount
					}

				}

			}
			if contains {
				break
			}
		}
	}
	if redirectUID != "" {
		redirectJob.LineNumber = redirectLine
		trackRedirectedJob(redirectJob, logPageDetails)
	}
}

func trackRedirectedJob(redirectJob printJob.PrintJob, logPageDetails *logPage) {
	var jobIDVersionNum []string
	(*logPageDetails).ReJob = redirectJob
	(*logPageDetails).HasRedirect = true
	for _, temp := range jobIDVersion {
		temp = strings.ReplaceAll(temp, "#", strconv.Itoa(redirectJob.IDNumber))
		jobIDVersionNum = append(jobIDVersionNum, temp)
	}
	for _, line := range AppLineList {
		for _, substring := range jobIDVersionNum {
			contains := false
			if strings.Contains(line.LineString, substring) {
				(*logPageDetails).LogLines[line.LineNumber] = line
			}
			if contains {
				break
			}
		}
	}

}
