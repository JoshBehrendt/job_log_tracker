import React, { Component } from "react";
import "./App.css";
import UploadForm from "./components/UploadForm";
import JobList from "./components/JobList";
import $ from "jquery";

class App extends Component {
    state = {
        page: 1, // 0: loading; 1: upload form; 2: job list; 3: job details
        loading: false,
        jobList: [],
        jobLogLines: undefined,
    }

    render() {
        switch (this.state.page) {
            case 1:
                console.log("yay im loading upload page")
                return ( 
                    <div className="App">
                        <UploadForm 
                          beginUploadCall={this.transitionToLoadingScreen} 
                          handleUploadDataCall={this.saveJobList} />
                    </div>
                )
            case 2:
                console.log("yay im loading job list page")
                return (
                    <div className="App">
                        <JobList jobList={this.state.jobList} logLines={this.state.jobLogLines} retrieveJobLines={this.retrieveJobLines} goBackToJobList={this.transitionToLoadingScreen}/>
                    </div>
                )
        }
    }
    
    transitionToLoadingScreen = () => {
        this.setState({page: 2});
        console.log("yay im in transitiona page");
    }

    saveJobList = (data) => {
      console.log("my state is being reset");
      this.setState({jobList : this.massageJobData(data)});
    }

    retrieveJobLines = (jobId) => {
      var updateLogLines = this.updateJobLines;
      $.get("/api/logLines/" + jobId, function(data, status) {
        updateLogLines(data);
      });
    }

    updateJobLines = (jobLines) => {
      this.setState({ jobLogLines: jobLines});
    }

    massageJobData(jobData) {    
        return jobData.map(function(printJob) {
          if (printJob["language"] == "") {
            printJob["language"] = "N/A";
          }
          return printJob;
        });
      };
}

export default App;
