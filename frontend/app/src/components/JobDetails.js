import React from "react";
import ReactTable from "react-table";
import 'react-table/react-table.css'

class JobDetails extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            showDetails: false,
            data: props.data,
            redirectData: props.redirectJob,
            columns: [
              {Header: "Job ID", accessor: "id", minWidth: 25},
              {Header: "Username", accessor: "username", minWidth: 80},
              {Header: "Document Name", accessor: "documentName"},
              {Header: "Printer", accessor: "printer"},
              {Header: "Pages", accessor: "pageCount", minWidth: 25},
              {Header: "Copies", accessor: "copyCount", minWidth: 25},
              {Header: "Color Pages", accessor: "colorPages", minWidth: 30},
              {Header: "Language", accessor: "language", minWidth: 40}
            ],
            redirectCols: [
                {Header: "Redirect Job ID", accessor: "id"},
                {Header: "Printer", accessor: "printer"},
                {Header: "Line Number", accessor: "lineNumber"},
                //releaseType false=Released At, true=Release by
                {Header: "Released At", accessor: "releaseInfo"},
                {Header: "Released By", accessor: "releaseInfo"}
            ]
        }
        
    }
    
    expandDetails = () => {
        this.setState({ showDetails: !this.state.showDetails });
    };
    
    render() {
        console.log("Redirect Array")
        console.log(this.state.redirectData)

        return (
            <section className="col--12 col--10--tablet col--8--laptop" 
                     style={{background:'rgba(255,255,255,1)'}}>
                         <button onClick={this.props.goBackToJobList}>back</button>
                <section className="bg--primary-grey-100">
                    <div className="container padding--vertical--none">
                        <div className="input-group">
                            <h4 className="type--feature--brand" style={{'padding-top': '2.5px'}}>Job Details</h4>
                            <a className="button button--icon" onClick={this.expandDetails}>
                                <img src="https://storage.googleapis.com/cdn1.papercut.com/web/img/icon/mono-bold/negative/chevron.svg" style={{transform: 'rotate(-90deg)', height: '16px'}} />
                            </a>
                        </div>
                    </div>

                    { this.state.showDetails ?
                        <section style={{background:'rgba(255,255,255,1)'}}>
                            <section className="type--align-center">
                                <ReactTable
                                    id="DetailsTable"
                                    data = {this.state.data}
                                    columns={this.state.columns}
                                    loading={this.state.loading}
                                    minRows={1}
                                    showPagination={false}
                                />
                            </section>
                            { this.state.redirectData.length != undefined?
                                <section>
                                    <div className="container padding--top--s padding--bottom--none">
                                        <div className="grid grid--left">
                                            <p className="color--brand">Redirected</p>
                                        </div>
                                    </div>
                                    <section className="type--align-center" className="padding--bottom--l">
                                        <ReactTable
                                            id="RedirectTable"
                                            data = {this.state.redirectData}
                                            columns={this.state.redirectCols}
                                            loading={this.state.loading}
                                            minRows={1}
                                            showPagination={false}
                                        />
                                    </section>
                                </section>
                            : null }
                        </section>
                        : null }
                </section>
            </section>
        );
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.data !== prevState.data || nextProps.redirectJob !== prevState.redirectData){
            return { data: nextProps.data, redirectData: nextProps.redirectJob};
        }
        else return null;
    }
}

export default JobDetails;
