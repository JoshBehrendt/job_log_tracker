import React from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import JobDetails from "./JobDetails";
import LogList from "./LogList";
import ReactDOM from "react-dom";
import Slider, { Range } from "rc-slider";
// We can just import Slider or Range to reduce bundle size
// import Slider from 'rc-slider/lib/Slider';
// import Range from 'rc-slider/lib/Range';
import "rc-slider/assets/index.css";

class JobList extends React.Component {
  constructor(props) {
    super(props); //since we are extending class Table so we have to use super in order to override Component class constructor
    console.log("being build with props: " + props);
    window.self = this;
    window.self.state = {
      //state is by default an object
      job: undefined,
      loading: true,
      columns: [
        { Header: "Job ID", accessor: "id", minWidth: 25 },
        { Header: "Username", accessor: "username", minWidth: 50 },
        { Header: "Document Name", accessor: "documentName", minWidth: 150 },
        { Header: "Printer", accessor: "printer", minWidth: 75 },
        { Header: "Job Time", accessor: "startTimeString", minWidth: 75 },
        { Header: "Line Number", accessor: "lineNumber", sortMethod: (a, b) => Number(a)-Number(b), minWidth: 50 },
        { Header: "Language", accessor: "language", minWidth: 40 },
        { Header: "Pages", accessor: "pageCount", minWidth: 25 },
        { Header: "Copies", accessor: "copyCount", minWidth: 25 },
        { Header: "Job UID", accessor: "jobUid", minWidth: 25 }
      ],
      data: props.jobList,
      shownData: props.jobList
    };
  }

  render() {
    if (this.state.job !== undefined) {
      console.log("sending job details: " + this.state.job);
      return (
        <section>
          <JobDetails data={this.state.job} 
          redirectJob={this.props.logLines !== undefined ? [this.props.logLines.redirectJob] : undefined}
          goBackToJobList={function() {
            window.self.setState({ job: undefined });
            window.self.props.goBackToJobList();
          }} />
          <hr className="margin--none" />
          <LogList data={this.props.logLines} />
        </section>
      );
    } else {
      return (
        <section>
          <section
            className="col--12 col--10--tablet col--8--laptop type--align-center"
            style={{ background: "rgba(255,255,255,1)" }}
          >
            <ReactTable
              id="jobTable"
              data={this.state.shownData}
              columns={this.state.columns}
              loading={this.state.loading}
              showPagination={false}
              pageSize = {this.state.pageSize}
              minRows={1}              
              sortable={true}
              multiSort={true}
              getTdProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: (e, handleOriginal) => {
                    // console.log('A Td Element was clicked!')
                    // console.log('it produced this event:', e)
                    // console.log('It was in this column:', column)
                    // console.log('It was in this row:', rowInfo)
                    // console.log('It was in this table instance:', instance)
                    this.setState({ job: [rowInfo.original] });
                    this.props.retrieveJobLines(rowInfo.original.jobUid);
                    
                    // IMPORTANT! React-Table uses onClick internally to trigger
                    // events like expanding SubComponents and pivots.
                    // By default a custom 'onClick' handler will override this functionality.
                    // If you want to fire the original onClick handler, call the
                    // 'handleOriginal' function.
                    if (handleOriginal) {
                      handleOriginal();
                    }
                  }
                };
              }}
            />
          </section>
          <footer
            style={{
              position: "fixed",
              left: 0,
              bottom: 0,
              height: "5%",
              width: "100%",
              backgroundColor: "white",
              textAlign: "center"
            }}
          >
            {/* <Range
              style={{ height: 50, top: "30%" }}
              defaultValue={[0, 100]}
              onChange={this.updateTable}
            /> */}
          </footer>
        </section>
      );
    }
  }

  updateTable(values) {
    const { first, last } = window.self.calculateFirstAndLast();
    var firstPosition = values[0];
    var secondPosition = values[1];

    var firstTime = new Date(first.replace(" ", "T") + "Z");
    var lastTime = new Date(last.replace(" ", "T") + "Z");

    var secondsBetweenDates = Math.abs(
      (lastTime.getTime() - firstTime.getTime()) / 1000
    );

    firstTime.setSeconds(
      firstTime.getSeconds() + secondsBetweenDates * (firstPosition / 100)
    );
    lastTime.setSeconds(
      lastTime.getSeconds() -
        secondsBetweenDates * ((100 - secondPosition) / 100)
    );

    var newJobList = window.self.state.data
      .map(element => {
        var currentTime = new Date(
          element["startTimeString"].replace(" ", "T") + "Z"
        );
        if (currentTime >= firstTime && currentTime <= lastTime) {
          return element;
        } else {
          return null;
        }
      })
      .filter(element => {
        return element != null;
      });

    // var startDate = new Date($("#start").html());
    // var endDate = new Date($("#end").html());

    window.self.setState({ shownData: newJobList });
    // calculate the time of first and time of last
    console.log("hey im updating the table: " + firstTime + lastTime);
  }

  calculateFirstAndLast() {
    var first = null;
    var last = null;
    window.self.state.data.forEach(element => {
      const startTimeString = element["startTimeString"];
      if (first == null || startTimeString < first) {
        first = startTimeString;
      }
      if (last == null || startTimeString > last) {
        last = startTimeString;
      }
    });
    return { first, last };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.jobList !== prevState.data) {
      var size = 0, key;
      for (key in nextProps.jobList) {
          if (nextProps.jobList.hasOwnProperty(key)) size++;
      }
      return { data: nextProps.jobList, pageSize: size, shownData: nextProps.jobList, loading: false };
    } else return null;
  }
}

export default JobList;
