import React from "react";
const localIpUrl = require('local-ip-url');

class UploadForm extends React.Component {
  state = {
    file: undefined
  };

  updateFile = e => {
    console.log("im saving file");
    this.setState({ file: e.target.files[0] });
  };

  sendRequest = () => {
    console.log("im sending request");
    this.props.beginUploadCall();
    return new Promise((resolve, reject) => {
      const req = new XMLHttpRequest();
      req.responseType = "json";

      const formData = new FormData();
      formData.append("upload log file", this.state.file, this.state.file.name);
      var ipaddress = "http://"+localIpUrl()+"/upload";
      console.log("My private Ip address is: " + localIpUrl('private'));
      req.open("POST", "http://127.0.0.1:8080/upload");
      var onLoadCall = this.props.handleUploadDataCall;
      req.onload = function() {
        console.log(req.response);
        onLoadCall(req.response);
      }
      let a = req.send(formData);
    });
  };

  render() {
    return (
        <section className="padding--top--l">
            <div className="container"  style={{background:'rgba(255,255,255,0.7)'}}>
                <div className="grid">
                    <div className="col--12 col--10--tablet col--8--laptop">
                        <h2>Upload Application Server Log File</h2>
                        <p className="type--feature--brand">
                            or diagnostic.zip
                        </p>
                        <div className="grid grid--reverse margin--top--xxxl">
                            <div className="control">
                                <input type="file" onChange={this.updateFile} name="upload log file" />
                                <button className="button" onClick={this.sendRequest}>Upload</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    );
  }
}




export default UploadForm;
