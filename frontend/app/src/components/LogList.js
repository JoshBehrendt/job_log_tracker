import React from "react";
import ReactTable from "react-table";
import createTable  from 'react-table-hoc-fixed-columns';
import 'react-table-hoc-fixed-columns/lib/styles.css'
import 'string-pixel-width'

const ReactTableFixedColumns = createTable (ReactTable);
var pixelWidth = require('string-pixel-width');


class LogList extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      columns: [
        {
          Header: "Time",
          accessor: "lineTime",          
          width: 150,
          fixed: true
        },
        {
          Header: "Line Number",
          accessor: "lineNo",
          sortMethod: (a, b) => Number(a)-Number(b),
          width: 100,
          fixed: true
        },
        {
          Header: "Content",
          accessor: "lineString",
          minWidth: 100,
          width: 100000,
          getProps: (state, rowInfo, column) => {
            return {
              style: { 
                //overflowX: "visible",
                textOverflow: "clip",
                textAlign: "left"
              }
            };
          }  
        }
      ],
      data: this.props.data
    };
  }

  convertDataToLogLines = data => {
    if (data === undefined) {
      console.log("No data")
      return;
    }

    var logLines = [];
    for (var key in data["logLines"]) {
      var line = data["logLines"][key];
      var stringTime = line["time"].slice(0, -1);
      var splitTime = stringTime.split(".");
      if(splitTime[splitTime.length-1].length==1)
      {
        stringTime = stringTime + "00";
      }
      else if(splitTime[splitTime.length-1].length==2)
      {
        stringTime = stringTime + "0";
      }
      if(splitTime.length ==1 )
      {
        stringTime = stringTime+".000";
      }
      stringTime = stringTime.replace("T", " ");
      logLines.push({
        lineTime: stringTime,
        lineNo: key,
        lineString: line["lineString"]
      });
    }
    console.log(logLines);
    return logLines;
  };
  
  render() {
    var jData = [];
    jData = this.convertDataToLogLines(this.state.data);
    return (      
      <div>
      
        {/* <section
          className="col--12 col--10--tablet col--8--laptop type--align-center"
          style={{  background: "rgba(255,255,255,1)"}}
        > */}
          <ReactTableFixedColumns
            id="logTable"
            minRows={1}
            data={jData}
            columns={[
              {
                Header: "Time",
                accessor: "lineTime",          
                width: 150,
                fixed: true
              },
              {
                Header: "Line Number",
                accessor: "lineNo",
                sortMethod: (a, b) => Number(a)-Number(b),
                width: 100,
                fixed: true
              },
              {
                Header: "Content",
                accessor: "lineString",
                minWidth: 100,
                width: this.state.cellWidth,
                headerStyle: {textAlign: 'left'},
                getProps: (state, rowInfo, column) => {
                  return {
                    style: { 
                      //overflowX: "visible",
                      textOverflow: "clip",
                      textAlign: "left"
                    }
                  };
                }  
              }
            ]}
            loading={this.state.loading}
            showPagination={false}  
            pageSize={this.state.pageSize}  
            style={{fontSize: 12, height: 700, background: "rgba(255,255,255,1)"}}
          />
        {/* </section> */}
      </div>
    );
  }
  

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.data !== prevState.data) {
      var size = 0, key;
      var maxLength = 0
      for (key in nextProps.data["logLines"]) {
          if (nextProps.data["logLines"].hasOwnProperty(key)){ 
            size++;
            var temp = pixelWidth(nextProps.data["logLines"][key]["lineString"], {size: 12});
            if(temp > maxLength)
              maxLength = temp;
          }
      }
      console.log("Window height: " + window.innerHeight);
      return { data: nextProps.data, loading: false, pageSize: size, cellWidth: maxLength};
    } else return null;
  }
}

export default LogList;
